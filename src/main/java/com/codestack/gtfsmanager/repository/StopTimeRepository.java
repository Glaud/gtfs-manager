package com.codestack.gtfsmanager.repository;

import com.codestack.gtfsmanager.entity.StopTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StopTimeRepository extends JpaRepository<String, StopTime> {
}
