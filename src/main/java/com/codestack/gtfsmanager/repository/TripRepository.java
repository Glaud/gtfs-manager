package com.codestack.gtfsmanager.repository;

import com.codestack.gtfsmanager.entity.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TripRepository extends JpaRepository<String, Trip> {
}
