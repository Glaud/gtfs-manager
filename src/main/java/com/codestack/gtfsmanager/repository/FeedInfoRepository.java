package com.codestack.gtfsmanager.repository;

import com.codestack.gtfsmanager.entity.FeedInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedInfoRepository extends JpaRepository<Long, FeedInfo> {
}
