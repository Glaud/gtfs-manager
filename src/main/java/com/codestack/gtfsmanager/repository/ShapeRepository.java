package com.codestack.gtfsmanager.repository;

import com.codestack.gtfsmanager.entity.Shape;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShapeRepository extends JpaRepository<String, Shape> {
}
