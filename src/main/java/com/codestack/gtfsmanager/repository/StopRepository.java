package com.codestack.gtfsmanager.repository;

import com.codestack.gtfsmanager.entity.Stop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StopRepository extends JpaRepository<Long, Stop> {
}
