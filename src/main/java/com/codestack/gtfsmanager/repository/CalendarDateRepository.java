package com.codestack.gtfsmanager.repository;

import com.codestack.gtfsmanager.entity.CalendarDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CalendarDateRepository extends JpaRepository<String, CalendarDate> {
}
