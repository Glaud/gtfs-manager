package com.codestack.gtfsmanager.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class StopTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    @NotNull
    @EqualsAndHashCode.Include
    private String tripId;
    @Column
    private LocalTime arrivalTime;
    @Column
    private LocalTime departureTime;
    @Column
    private Long stopSequence;
    @Column
    private Integer pickupType;
    @Column
    private Integer dropOff;
    @Column
    private String stopHeadsign;
    @ManyToOne
    @JoinColumn(name = "STOP_ID")
    private Stop stop;
}
