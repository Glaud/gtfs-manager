package com.codestack.gtfsmanager.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Stop {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    @NotNull
    @EqualsAndHashCode.Include
    private Long id;
    @Column
    private String name;
    @Column
    private String lat;
    @Column
    private String lon;
    @Column
    private Long code;
}
