package com.codestack.gtfsmanager.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    @NotNull
    @EqualsAndHashCode.Include
    private Long id;
    @Column
    private String shortName;
    @Column
    private String longName;
    @Column
    private String desc;
    @Column
    private String type;
    @Column
    private String color;
    @Column
    private String textColor;
    @OneToMany(mappedBy = "route")
    private List<Agency> agencies;
    @OneToMany(mappedBy = "route")
    private List<Route> routes;

}