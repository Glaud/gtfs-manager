package com.codestack.gtfsmanager.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Shape {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    @NotNull
    @EqualsAndHashCode.Include
    private String id;
    @Column
    private String ptLat;
    @Column
    private String ptLon;
    @Column
    private Long ptSequence;
    @OneToMany(mappedBy = "shape")
    private List<Shape> shapes;
}
