package com.codestack.gtfsmanager.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class CalendarDate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    @NotNull
    @EqualsAndHashCode.Include
    private String serviceId;
    @Column
    private LocalDateTime date;
    @Column
    private Integer exceptionType;
}
