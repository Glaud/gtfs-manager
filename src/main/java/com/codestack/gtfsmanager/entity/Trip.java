package com.codestack.gtfsmanager.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Trip {

    //    route_id,service_id,trip_id,trip_headsign,trip_short_name,direction_id,shape_id,wheelchair_accessible
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    @NotNull
    @EqualsAndHashCode.Include
    private String id;
    @Column
    private String serviceId;
    @Column
    private String headsign;
    @Column
    private String shortName;
    @Column
    private boolean wheelchairAccessible;
    @ManyToOne
    @JoinColumn(name = "ROUTE_ID")
    private Route route;
    @ManyToOne
    @JoinColumn(name = "SHAPE_ID")
    private Shape shape;

}
