package com.codestack.gtfsmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GtfsManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GtfsManagerApplication.class, args);
	}

}
